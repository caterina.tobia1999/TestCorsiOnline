import java.util.ArrayList;
// example of overloaded constructor
public class Pizza {
    String impasto;
    String salsa;
    String formaggio;
    String extra;
    String extra2;
    static ArrayList<Pizza> pizze = new ArrayList<Pizza>();
    static int numeroPizze;

    // focaccia
    Pizza (String impasto){
        this.impasto = impasto;
        System.out.println("Focaccia " + this.impasto);
        numeroPizze++;
        pizze.add(this);
    }
    // margherita
    Pizza (String impasto, String salsa, String formaggio){
        this.impasto = impasto;
        this.salsa = salsa;
        this.formaggio = formaggio;
        System.out.println("Margherita " + this.impasto + " con " + this.salsa + " e " + this.formaggio);
        numeroPizze++;
        pizze.add(this);
    }
    // normale
    Pizza (String impasto, String salsa, String formaggio, String extra){
        this.impasto = impasto;
        this.salsa = salsa;
        this.formaggio = formaggio;
        this. extra = extra;
        System.out.println("Pizza " + this.impasto + " con " + this.salsa + ", " + this.formaggio + " e " + this.extra);
        numeroPizze++;
        pizze.add(this);
    }
    // 2 extra
    Pizza (String impasto, String salsa, String formaggio, String extra, String extra2){
        this.impasto = impasto;
        this.salsa = salsa;
        this.formaggio = formaggio;
        this. extra = extra;
        this. extra2 = extra2;
        System.out.println("Pizza " + this.impasto + " con " + this.salsa + ", " + this.formaggio + ", " + this.extra + " e " + this.extra2);
        numeroPizze++;
        pizze.add(this);
    }

    // sovrascrivo (override) metodo preesistente che da l'indirizzo
    public String toString(){
        String infoString = "Ingredienti: " + this.impasto;
        if (this.salsa != null){
            infoString += ", " + this.salsa;
            if (this.formaggio != null){
                infoString += ", " + this.formaggio;
                if (this.extra != null){
                    infoString += ", " + this.extra;
                    if (this.extra2 != null){
                        infoString += ", " + this.extra2;
                    }
                }
            }
        }
        return infoString;
    }
}
