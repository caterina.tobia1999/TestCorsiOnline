import java.util.ArrayList;
public class Studente extends Persona{
    String classe;
    String materiaPreferita;
    ArrayList<Integer> votiStoria;
    static int numeroStudenti;

    Studente(String nome, String cognome, int eta, String colorePreferito, String classe, String materiaPreferita, ArrayList<Integer> votiStoria) {
        super(nome, cognome, eta, colorePreferito);
        this.classe = classe;
        this.materiaPreferita = materiaPreferita;
        this.votiStoria = votiStoria;
        numeroStudenti++;
    }
    @Override
    void saluta(){
        System.out.println("Boun giorno prof!");
    }

    void studia(){
        System.out.println("sto studiando " + this.materiaPreferita);
    }
}
