public class Macchina extends Veicolo implements Motore {
    String marca;
    String modello;
    String targa;
    static int numeroMacchine;
    Macchina(String marca, String modello, String targa){
        this.marca = modello;
        this.modello = modello;
        this.targa = targa;
        numeroMacchine ++;
    }

    @Override
    public void siMuove() {
        System.out.println("la macchina si muove");
    }

    @Override
    public void accensione() {
        System.out.println("si accende");
    }

    @Override
    public void spegnimento() {
        System.out.println("si spegne");
    }
}
