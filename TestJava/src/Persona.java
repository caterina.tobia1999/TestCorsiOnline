import java.util.ArrayList;

public class Persona {
    // le variabili definite qui sono GLOBALI,
    // possono essere assegnate con il this a istanze diverse e poi usate in tutti i metodi di questa classe
    static int numeroPersone;
    private String nome;
    private String cognome;
    int eta;
    String colorePreferito;

    // costruttore
    Persona(String nome, String cognome, int eta, String colorePreferito){
        this.nome = nome;
        this.cognome = cognome;
        this.eta = eta;
        this.colorePreferito = colorePreferito;
        numeroPersone++;
       // System.out.println("numeroPersone = " + numeroPersone);
    }

    public String getNome(){
        return nome;
    }
    public String getCognome(){
        return cognome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public void setCognome(String cognome){
        this.cognome = cognome;
    }
    public void copiaPersona(Persona persona){
        this.setNome(persona.getNome());
        this.setCognome(persona.getCognome());
    }
    void saluta(){
        System.out.println("Ciao!");
    }
    void Presentati(){
        System.out.println("Ciao, sono " + this.nome + " " + this.cognome + " di " + this.eta +
                " anni, il mio colore preferito è il " + this.colorePreferito);
    }

    void salutaPersona(Persona personaDaSalutare){
        System.out.println("Ciao " + personaDaSalutare.nome + " sono " + this.nome);
    }

    // sovrascrivo (override) metodo preesistente che da l'indirizzo
    @Override
    public String toString(){
        String infoString = "Info Persona: " + this.nome + ", " + this.cognome + ", " + this.eta + ", " + this.colorePreferito;
        return infoString;
    }
    /* questa è il metodo originale
    public String toString() {
        return getClass().getName() + "@" + Integer.toHexString(hashCode());
    }
    */
}
