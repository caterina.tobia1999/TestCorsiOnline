import java.io.*;
import java.util.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.text.DateFormat;

public class Main {
    public static void main(String[] args) {

        // -------------------------------------- Persone
        Persona persona1 = new Persona("Luca", "Rossi", 23, "blu");
        Persona persona2 = new Persona("Marco", "Verdi", 35, "verde");
        Persona persona3 = new Persona("Dennis", "Barchetti", 24, "giallo" );

        System.out.println();
        System.out.println("Classe Persona");
       // System.out.println(persona1.toString()); // .toString() -> @OVERRIDE sovrascritto con info istanze
        Persona[] persone = {persona1, persona2, persona3};
        for(int i=0; i<persone.length; i++) {
            System.out.println(persone[i]);
        }

        System.out.println(persona1.getNome());
        // System.out.println("Ciao " + persona1.nome + " " + persona1.cognome);
        persona2.saluta();
        persona3.salutaPersona(persona1);
        System.out.println("numero totale persone = " + Persona.numeroPersone);

        // -------------------------------------- Studenti e insegnanti
        ArrayList<Integer> votiStoria1 = new ArrayList<Integer>();
        Collections.addAll(votiStoria1, 9,8,9,7,9);
        Studente studente1 = new Studente("Luca", "Rossi", 14, "blu", "2G", "Storia", votiStoria1);

        ArrayList<Integer> votiStoria2 = new ArrayList<Integer>();
        Collections.addAll(votiStoria2, 8,9,7,9,7);
        Studente studente2 = new Studente("Anna", "Verdi", 14, "blu", "2G", "Matematica", votiStoria2);

        String[] classiIns1 = {"1G", "2G", "3G"};
        Insegnante insegnante1 = new Insegnante("Matteo", "Neri", 23, "verde", "Matematica", classiIns1);

        System.out.println();
        System.out.println("Classi studenti e insegnanti, figli di persona:\n salutano entrambi, ma in modo diverso, in oltre solo lo studente studia e solo l'insegnante insegna");
        // polimorfismo (studenti e insegnanti fanno parte di una classe e sono entrambi persone)
        Persona[] classe = {studente1, studente2, insegnante1}; // ,
        for (Persona persona: classe){
            persona.saluta();
        }
        studente1.studia();
        insegnante1.insegna();
        System.out.println("Persona.numeroPersone è un contantore static int che INCREMENTA anche nelle classi figlie!!\n non va messo anche nei costruttori delle figlie perchè LO CHIAMI GIà nel SUPER");
        System.out.println("numero totale persone = " + Persona.numeroPersone + " di cui " + Insegnante.numeroInsegnanti + " insegnanti e " + Studente.numeroStudenti + " studenti");

        // -------------------------------------- Pizze
        System.out.println();
        System.out.println("Overloaded constructor, stesso costruttore riconosciuto in base agli attributi per numero o tipo di dato :\n pizze, nmumero di ingredienti variabile");

        Pizza pizza1 = new Pizza("integrale");
        Pizza pizza2 = new Pizza("integrale", "pomodoro", "mozzarella");
        Pizza pizza3 = new Pizza("integrale", "pomodoro", "mozzarella", "prosciutto");
        Pizza pizza4 = new Pizza("integrale", "pomodoro", "mozzarella", "prosciutto", "funghi");
        System.out.println("Numero di pizze ordinate: " + Pizza.numeroPizze);
        for(Pizza pizza:Pizza.pizze) {
            System.out.println(pizza);
        }


        // -------------------------------------- Macchine
        System.out.println();
        System.out.println("Classi astratte e interfeccie: macchine");
        Macchina macchina1 = new Macchina("Nissan", "Micra", "DX096M");
        macchina1.accensione();
        macchina1.siMuove();

        // -------------------------------------- leggere e scrivere da file:
        System.out.println();
        System.out.println("Leggere e scrivere da file:");
        File file = new File("prova.txt");
        try  {
            FileWriter writer = new FileWriter("prova.txt");
            writer.write("Questo testo sovrascrive testo pre-esistente \n");
            writer.append("Questo testo non sovrascrive ma viene aggiunto al testo pre-esistente \n");
            writer.close();
        }catch (IOException e) {
            e.printStackTrace();
        }

        if(file.exists()){
            System.out.println("il file esiste, ed è lungo: " + file.length());
            System.out.println("percorso relativo: " + file.getPath());
            System.out.println("percorso assoluto: " + file.getAbsolutePath());
        }

        try  {
            FileReader reader = new FileReader("prova.txt");
            int data = reader.read();
            System.out.println("Ascii: " +data + ", Char: " + (char)data);
            while(data != -1){
                System.out.print((char)data);
                data = reader.read();
            }
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

        // -------------------------------------- Date and time:
        LocalDate x = LocalDate.now();
        LocalTime y = LocalTime.now();
        LocalDateTime z = LocalDateTime.now();

        DateTimeFormatter dateFormatter1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter dateFormatter2 = DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy");

        System.out.println(x + " "  + y + " "  + z);
        System.out.println(x.format(dateFormatter1));
        System.out.println(x.format(dateFormatter2));
        // -------------------------------------- HashMap = dizionario/chiave:valore
        HashMap<String,String> capitali = new HashMap<String,String>();
        capitali.put("Italia","Roma");
        capitali.put("Germania","Berlino");
        capitali.put("Francia","Parigi");
        capitali.put("Inghilterra","Londra");

        System.out.println();
        System.out.println("HashMap = dizionario/chiave:valore:");
        System.out.println(" Capitali: " + capitali);

        capitali.remove("Francia");
        // capitali.clear();
        System.out.println(" Capitali -1: " + capitali);

        // -------------------------------------- iteratori ArrayList
        ArrayList<String> gatti = new ArrayList<String>();
        gatti.add("Alfuccia");
        gatti.add("Mamo");
        gatti.add("Lucy");
        gatti.add("Acico");
        gatti.add("Lauro");

        System.out.println();
        System.out.println("Iteratori e ArrayLis:");
        System.out.println(" I gatti: " + gatti + " sono " + gatti.size());

        Iterator<String> itGatti = gatti.iterator();
        while(itGatti.hasNext()){
           // System.out.println(itGatti.next());
            if(itGatti.next() == "Mamo"){
                itGatti.remove();
            }
        }
        System.out.println(" I gatti -1:" + gatti + " sono " + gatti.size());

        ArrayList<Integer> votiMatematica = new ArrayList<Integer> ();
        votiMatematica.add(0,9);
        votiMatematica.add(8);
        votiMatematica.add(9);
        System.out.println(" Voti matematica:" + votiMatematica);

        // -------------------------------------- Try e catch ECCEZIONI I/O -> inserici 0 come denominatore o un carattere.
        Scanner scanner = new Scanner(System.in);
        System.out.println();
        System.out.println("Divisioni: Input");
            try {
                System.out.println("inserisci un numero al numeratore");
                int a = scanner.nextInt();
                System.out.println("inserisci un numero al denominatore");
                int b = scanner.nextInt();
                float result = (float) a/b;
                System.out.println("il risultato di " + a + ":" + b + " è " + result);
            } catch (ArithmeticException e0) {
                System.out.println("Non inserire 0 al denominatore!! la divisione risulta impossibile");
            } catch (InputMismatchException eI) {
                System.out.println("Non inserire caratteri!! le divisioni si fanno tra numeri");
            }

    }
}