public class Insegnante extends Persona {
    String materia;
    String[] classiIns;
    static int numeroInsegnanti;

    Insegnante(String nome, String cognome, int eta, String colorePreferito, String materia, String[] classiIns) {
        super(nome, cognome, eta, colorePreferito);
        this.materia = materia;
        this.classiIns = classiIns;
        numeroInsegnanti++;
    }
    @Override
    void saluta(){
        System.out.println("Boun giorno ragazzi!");
    }
    void insegna(){
        System.out.println("sto insegnando " + this.materia);
    }
}
